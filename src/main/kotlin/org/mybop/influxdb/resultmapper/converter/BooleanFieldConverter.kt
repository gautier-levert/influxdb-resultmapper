package org.mybop.influxdb.resultmapper.converter

interface BooleanFieldConverter<T> : FieldConverter<T, Boolean?, Boolean?>

package org.mybop.influxdb.resultmapper.converter

interface StringFieldConverter<T> : FieldConverter<T, String?, String?>

package org.mybop.influxdb.resultmapper.converter.basics.tag

import org.mybop.influxdb.resultmapper.converter.TagConverter
import kotlin.reflect.KType
import kotlin.reflect.full.createType

internal class StringConverter : TagConverter<String?> {

    override fun supportedType() = String::class.createType(nullable = true)

    override fun convert(key: String?) = key

    override fun reverse(key: String?, type: KType) = key
}

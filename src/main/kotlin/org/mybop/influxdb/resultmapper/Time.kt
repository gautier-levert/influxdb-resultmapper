package org.mybop.influxdb.resultmapper

import org.mybop.influxdb.resultmapper.converter.TimeConverter
import kotlin.reflect.KClass

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
annotation class Time(
        val name: String = "time",
        val converter: KClass<out TimeConverter<*>> = TimeConverter::class
)

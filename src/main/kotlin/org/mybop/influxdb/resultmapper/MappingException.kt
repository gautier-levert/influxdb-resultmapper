package org.mybop.influxdb.resultmapper

class MappingException(
        message: String,
        cause: Throwable? = null
) : RuntimeException(message, cause)

package org.mybop.influxdb.resultmapper

enum class Strategy {
    COMPLETE,
    SIMPLE
}

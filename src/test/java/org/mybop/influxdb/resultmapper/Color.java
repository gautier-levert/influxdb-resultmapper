package org.mybop.influxdb.resultmapper;

public enum Color {
    RED,
    BLUE,
    GREEN
}
